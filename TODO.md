# Distinguishing clusters
  - Calculate Euclidean distances between all words that start with 'has/is' (or are quoted strings, or are plurals, or start/end/both with '_') and calculate similar distances all possible pairs of words. Do a t-test!
  - Try labeling different clusters by whatever obvious property they show. Maybe Google to see if those identifiers belong to a particular type.
  - Why are there so many Soot calls? What is Soot's role in android app development?
  - Add a search bar!