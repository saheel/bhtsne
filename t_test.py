import os, sys, re, numpy
import pandas as pd
from scipy.stats import ttest_ind

if len(sys.argv) not in [5, 6]:
    print('Usage: python t_test.py <input_embeddings_file> <output_embeddings_file> <vocabulary_file> <regex1> [<regex2>]')
    print('One embedding per line in embeddings file. One word per line in vocab. Make sure #embeddings = #words in the vocab.')
    print('Last argument is optional; if given, I will do t-test on [distances between words matching <regex1>] and [distances between words matching <regex2>], otherwise I will do a t-test on [distances between words matching <regex1>] and [distances between all the vocab words].')
    sys.exit()

in_emb_fpath = sys.argv[1]
out_emb_fpath = sys.argv[2]
vocab_fpath = sys.argv[3]
regex1 = sys.argv[4]

if len(sys.argv) == 6:
    regex2 = sys.argv[5]
else:
    regex2 = '.*'

# Read in the input and output embeddings
in_embs = pd.read_csv(in_emb_fpath, sep='\t', index_col=False, header=None)
out_embs = pd.read_csv(out_emb_fpath, sep='\t', index_col=False, header=None)

# Read in the vocabulary line-by-line 
# (bulk reading fails due to some ugly vocab words messing up with the delimiter/newline chars)
labels = []
with open(vocab_fpath, 'rb') as vocab_file:
    for index, line in enumerate(vocab_file):
        labels.append(line[:-1]) # `-1` takes out the '\n' char present at the end of each vocab word

# Sanitary check
if len(set([len(labels), len(in_embs), len(out_embs)])) != 1:
    print('Mismatch in number of embeddings and the vocab size. Aborting...')
    print('Vocab:   ' + str(len(labels)))
    print('In_Emb:  ' + str(len(in_embs)))
    print('Out_Emb: ' + str(len(out_embs)))
    sys.exit()

# Keep only those vocab words (and corresponding embeddings) that contain `keyword`
regex1_indices = [index for index, label in enumerate(labels) 
                  if re.search(regex1, label) is not None]
regex2_indices = [index for index, label in enumerate(labels) 
                  if re.search(regex2, label) is not None]

regex1_labels = [labels[index] for index in regex1_indices]
regex1_in_embs = in_embs[in_embs.index.isin(regex1_indices)]
# regex1_out_embs = out_embs[out_embs.index.isin(regex1_indices)]

regex2_labels = [labels[index] for index in regex2_indices]
regex2_in_embs = in_embs[in_embs.index.isin(regex2_indices)]
# regex2_out_embs = out_embs[out_embs.index.isin(regex2_indices)]

print('%d words for %s' % (len(regex1_indices), regex1))
print('%d words for %s' % (len(regex2_indices), regex2))

# We are ignoring out_embs for now
regex1_in_embs_np_arrs = []
for row in regex1_in_embs.iterrows():
    regex1_in_embs_np_arrs.append(numpy.array(row[1]))

regex2_in_embs_np_arrs = []
for row in regex2_in_embs.iterrows():
    regex2_in_embs_np_arrs.append(numpy.array(row[1]))

regex1_distances = []
for i, np_arr_i in enumerate(regex1_in_embs_np_arrs):
    if i < len(regex1_in_embs_np_arrs) - 1:
        for np_arr_j in regex1_in_embs_np_arrs[(i+1):]:
            regex1_distances.append(numpy.linalg.norm(np_arr_i - np_arr_j))
print(str(len(regex1_distances)) + ' regex1_distances calculated.')

regex2_distances = []
for i, np_arr_i in enumerate(regex2_in_embs_np_arrs):
    if i < len(regex2_in_embs_np_arrs) - 1:
        for np_arr_j in regex2_in_embs_np_arrs[(i+1):]:
            regex2_distances.append(numpy.linalg.norm(np_arr_i - np_arr_j))
print(str(len(regex2_distances)) + ' regex2_distances calculated.')

t, prob = ttest_ind(regex1_distances, regex2_distances)
print('\nt-statistic and p value: ')
print(t)
print(prob)
