import os, sys, re
import matplotlib.pyplot as plt
import pandas as pd
from mpldatacursor import datacursor

if len(sys.argv) < 4:
    print('Usage: python plot.py <2D_input_embeddings_file> <2D_output_embeddings_file> <vocabulary_file> [--regex <regex>]')
    print('One embedding per line in embeddings file. One word per line in vocab. Make sure #embeddings = #words in the vocab.')
    print('Last argument is optional; if given, I will only plot vocab words matching <regex>.')
    sys.exit()

in_emb_fpath = sys.argv[1]
out_emb_fpath = sys.argv[2]
vocab_fpath = sys.argv[3]

if '--regex' in sys.argv:
    regex = sys.argv[sys.argv.index('--regex') + 1]
else:
    regex = '.*'

# Read in the input and output embeddings
in_embs = pd.read_csv(in_emb_fpath, sep='\t', names = ['x', 'y'], index_col=False)
out_embs = pd.read_csv(out_emb_fpath, sep='\t', names = ['x', 'y'], index_col=False)

# Read in the vocabulary
labels = []
with open(vocab_fpath, 'rb') as vocab_file:
    for index, line in enumerate(vocab_file):
        labels.append(line[:-1]) # `-1` takes out the '\n' char present at the end of each vocab word

# Sanitary check
if len(set([len(labels), len(in_embs), len(out_embs)])) != 1:
    print('Mismatch in number of embeddings and the vocab size. Aborting...')
    print('Vocab:   ' + str(len(labels)))
    print('In_Emb:  ' + str(len(in_embs)))
    print('Out_Emb: ' + str(len(out_embs)))
    sys.exit()

# Keep only those vocab words (and corresponding embeddings) that contain `keyword`
valid_indices = [index for index, label in enumerate(labels) 
                 if re.search(regex, label) is not None]

key_labels = [labels[index] for index in valid_indices]
key_in_embs = in_embs[in_embs.index.isin(valid_indices)]
key_out_embs = out_embs[out_embs.index.isin(valid_indices)]

# Plotting input embeddings
fig, ax = plt.subplots()
ax.set(title='Input embeddings for words matching `%s`' % regex)
ax.plot(key_in_embs.x, key_in_embs.y, 'bo', markersize=0.7)
ax.margins(0.1)
datacursor(axes=ax, hover=True, point_labels=key_labels, bbox=dict(alpha=1, fc='white'))

fig, ax = plt.subplots()
ax.set(title='Input embeddings')
ax.plot(in_embs.x, in_embs.y, 'bo', markersize=0.7)
ax.margins(0.1)
datacursor(axes=ax, hover=True, point_labels=labels, bbox=dict(alpha=1, fc='white'))

# # Plotting output embeddings
# fig, ax = plt.subplots()
# ax.set(title='Output embeddings')
# ax.plot(out_embs.x, out_embs.y, 'ro')
# ax.margins(0.1)
# datacursor(axes=ax, hover=True, point_labels=labels, bbox=dict(alpha=1, fc='white'))

plt.show()
