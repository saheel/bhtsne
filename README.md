# Notes

This repo is a fork of [t-sne](https://github.com/lvdmaaten/bhtsne). I am going to use it to visualize the embeddings of various neural networks.

# Usage

Use this to project your input/output embeddings to 2D space:

`python bhtsne.py -d 2 -i <embeddings_file> -o <2D_proj_file>`

Use this to generate a clickable, zoomable, scatter plot:

`python plot.py <in_emb_2D_proj_file> <out_emb_2D_proj_file> <vocab_sorted_by_freq>`

More (interesting) plots coming soon! If your vocab is not sorted by frequency, the numbers in the annotation boxes won't make sense, but you will still get to see the labels of the points.

# Dependencies

Tested with Python 2 on Linux. Should work elsewhere too. Depends on these Python packages: matplotlib, mpldatacursor, pandas. Use this to install them:

`sudo pip install <package>`